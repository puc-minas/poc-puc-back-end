<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api']], function () {
    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

    Route::resource('/clients', 'Api\ClientsController');
    Route::resource('/providers', 'Api\ProvidersController');
    Route::resource('/products', 'Api\ProductsController');
});
Route::resource('/store', 'Api\ProductsController');
